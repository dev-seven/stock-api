// Interface for exposing controllers to routes

let productsController = require('./controllers/controllers.products');
let stockController = require('./controllers/controllers.customers.stock');
let customersController = require('./controllers/controllers.customers');
let controllers = { customers: customersController, products: productsController, stock: stockController };

exports.customers = controllers.customers;
exports.products = controllers.products;
exports.stock = controllers.stock;

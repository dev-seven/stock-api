
let controllers = require('./controllers');

let routes = function(app) {

  // Developer Discovery
  app.get('/', function(req, res) {
    res.json({ link: { href: '/api' } } );
  });

  app.get('/api', function(req, res) {
    res.json({ products : { link: { href: '/api/products' } }, stock: { link: { href: '/api/stock' } }, customers: { link: { href: '/api/stock' } }});
  });

  // Application Routes
  app.route('/api/products').get(controllers.products.all);
  app.route('/api/products/:name').get(controllers.products.name);

  app.route('/api/customers').get(controllers.customers.all);
  app.route('/api/customers/:customer').get(controllers.customers.customer).post(controllers.customers.publish);
  app.route('/api/customers/:customer/stock').get(controllers.customers.stock);
  app.route('/api/customers/:customer/stock/:filter').get(controllers.customers.stockFilter);
};
 
module.exports = routes;
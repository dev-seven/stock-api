var express = require("express"),
bodyParser = require("body-parser"),
cors = require('cors'),
app = express(),
port = process.env.PORT || 3000;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
 
var routes = require("./routes.js")(app);
 
var server = app.listen(port, function () {
  console.log("G&B REST API server listening on port %s", port);
});

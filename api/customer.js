const fs = require('fs');
const fetch = require("node-fetch");

const stock =  require('./customer.stock');
const products =  require('./products');

let customers = {};

customers.all = function() {
	let customerData = fs.readFileSync('./customers.json');
	return JSON.parse(customerData);
};

customers.customer  = function(customer) {
	let customerData = customers.all();
	let customerFilter = function (availableCustomer) {
		return availableCustomer.ref.toLowerCase() === customer.toLowerCase();
	};
	let customerResult =  customerData.filter(customerFilter);
	let customerResponse = customerResult.length ? customerResult[0] : {};

	return customerResponse;
};

customers.publish  = async function(customerParam, stock) {
	let customer = customers.customer(customerParam);
	let feed = customer.feed;
	let model = customer.feed.model;
	let payload = customers.setFeedData(stock, model);

	let endpoint = feed.endpoint + feed.auth;

	let requestObject = {
        method: feed.method,
        body:    JSON.stringify(payload),
        headers: { 'Content-Type': 'application/json' },
    };

    let result;
	try {
    	const response = await fetch(feed.endpoint + feed.auth, requestObject);
    	result = await response;
  	} catch (error) {
    	result = {error: error};
  	}

	return {status: result.status, message: result.statusText, payload: payload};
};

customers.stock = async function(customerParam) {
	let customer = customers.customer(customerParam);
	let account = customer.account;
	let customerStock = await stock.all(account);
	return customerStock;
};

customers.stockFilter = async function(customerParam, filter) {
	let customer = customers.customer(customerParam);
	let account = customer.account;
	let customerFilteredStock = await stock.colour(filter, account);
	return customerFilteredStock;
};

customers.setFeedData = function(stock, model) {
	let feedData = [];
	// here we would benefit from Strongly defining the Type for 'stock'
	// Uses TypeScript ( idiot )
	/* TypeOf 'stock'
	{
		productNumber: String,
		physicalStock: Number,
		physicalReserved: Number,
		orderedInTotal: Number,
		onOrder: Number,
		totalAvailable: Number
		}
	*/

	let modelKeys = Object.keys(model);

	stock.forEach(function(stockItem) {
		let feedDataItem = {};
		modelKeys.forEach(function(modelKey) {
			feedDataItem[modelKey] = stockItem[model[modelKey]];
		});
		feedData.push(feedDataItem);
	});

	return feedData;
};

exports.all = customers.all;
exports.customer = customers.customer;
exports.publish = customers.publish;
exports.stock = customers.stock;
exports.stockFilter = customers.stockFilter;


const fs = require('fs');
const fetch = require("node-fetch");
const stockEndpoint = 'http://paperr-stock-api.azurewebsites.net/api/StockFeed/gbus/'; /* TODO: config file */

let products = require('./products');
let stock = {};

stock.all = async (account) => {
	let stockData = [];

	try {
    	const response = await fetch(stockEndpoint + account);
    	stockData = await response.json();
  	} catch (error) {
    	console.log(error);
  	}

	return stockData;
};



stock.byProductData = function(productData, stockItem) {
	return productData.filter(function(product) {
		return product.productNumber === stockItem.productNumber;
	}).length > 0;
};

stock.colour = async function(colour, account) {
	let stockData = await stock.all(account);
	let productData = await products.colour(colour);
	let filteredStock = stockData.filter(function(stockItem) {
		return stock.byProductData(productData, stockItem);
	});

	let filterResponse = stock.setFilterResponse(productData, filteredStock, colour);

	return filterResponse;
};

stock.setFilterResponse = function(productData, filteredStock, filter) {
	let productsNotInAccount = productData.length - filteredStock.length;
	let filterResponse = {
		filter : { colour: filter},
		products: { total: productData.length, data: { link: { href: '/api/products?colour=' + filter } } },
		stock : { total: filteredStock.length, data: filteredStock },
		productsNotInAccount: { total: productsNotInAccount }
	};
	return filterResponse;
};

exports.all = stock.all;
exports.colour = stock.colour;
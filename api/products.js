const fs = require('fs');
const fetch = require("node-fetch");
const productEndpoint = 'http://paperr-product-api.azurewebsites.net/api/Product/gbus'; /* config file */

let products = {};
products.all = async () => {
	let productData = [];

	try {
    	const response = await fetch(productEndpoint);
    	productData = await response.json();
  	} catch (error) {
    	console.log(error);
  	}

	return productData;
};

products.number  = function(number, done) {
	let productData = products.all( (data) => {
		done();
	});

	return productData.filter(productFilter);
};

products.colour  = async (colour) => {
	let productData = await products.all();
	let productFilter = (product) => {
		return product.colourGroup.toLowerCase() === colour.toLowerCase();
	};

	return  productData.filter(productFilter);
};

products.byStockData = function(stockData,  productItem) {
	return stockData.filter(function(stockItem) {
		return productItem.productNumber === stockItem.productNumber;
	}).length > 0;
};

products.customer = function(stockData) {
	let productData = products.all();
	let filteredProducts = productData.filter(function(productItem) {
		return products.byStockData(stockData,  productItem);
	});

	return filteredProducts;
};


exports.all = products.all;
exports.number = products.number;
exports.colour = products.colour;
exports.customer = products.customer;

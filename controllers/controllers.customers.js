const fs = require('fs');

let customers = require('../api/customer');
let controller = {};

controller.customer = function(req, res) {
	let customerData  = customers.customer(req.params.customer);
	let customer = customerData.name ? controller.setSafeResponse(customerData) : {};
    res.json(customer);
};

controller.setSafeResponse = function(customerData) {
	return {
		name: customerData.name,
		ref: customerData.ref,
		link: { href: '/api/customer/' + customerData.ref}
	};
};

controller.setSafeResponses = function(customersData) {
	// no feed expose to endpoint return
	let response = [];

	customersData.forEach(function(customerData) {
		let customer = controller.setSafeResponse(customerData);
		response.push(customer)
	});

	return response;
};

controller.all = function(req, res) {
	let customersData = customers.all();
	let publicData = controller.setSafeResponses(customersData);

    res.json(publicData);
};

controller.stock = async function(req, res) {
	let customerParam = req.params.customer;
	let customerStock = await customers.stock(customerParam);
    res.json(customerStock);
};

controller.stockFilter = async function(req, res) {
	let customerParam = req.params.customer;
	let filter = req.params.filter;
	let customerStockFiltered = await customers.stockFilter(customerParam, filter);
    res.json(customerStockFiltered);
};

controller.publish = async function(req, res) {
	let stock  = req.body;
	let customerParam = req.params.customer;
	let customerResponseFeedData = await customers.publish(customerParam, stock);
   	res.json(customerResponseFeedData);
};

exports.all = controller.all;
exports.customer = controller.customer;
exports.publish = controller.publish;
exports.stock = controller.stock;
exports.stockFilter = controller.stockFilter;



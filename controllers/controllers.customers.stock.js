

let stock = require('../api/customer.stock');

let controller = {};

controller.fetch = function() {
	return stock.all();
};

controller.all = async function(req, res) {
	let stockResponse = await controller.fetch();
	res.json(stockResponse);
};

controller.colour = async function(req, res) {
	let stockResponse = await stock.colour(req.params.colour);
    res.json(stockResponse);
};

exports.all = controller.all;
exports.colour = controller.colour;
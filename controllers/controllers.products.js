let products = require('../api/products');

let controller = {};

let hasQuery = function(req) {
	return req.query && Object.keys(req.query).length > 0;
};

controller.fetch = function() {
	return products.all();
};

controller.name = function(req, res) {
	let productsResponse = products.number(req.params.name);
    res.json(productsResponse);
};

controller.colour = function(colour) {
	return products.colour(colour);
};

controller.filter = function(query) {
	// Interface step for query params ( just has one 'colour' so return it )
	return controller.colour(query.colour);
};

controller.all = async function(req, res) {
	let productsResponse = hasQuery(req) ? await controller.filter(req.query) : await controller.fetch();
	res.json(productsResponse);
};

exports.all = controller.all;
exports.name = controller.name;